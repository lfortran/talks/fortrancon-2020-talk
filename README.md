# FortranCon 2020 Talk

Abstract and presentation about LFortran for FortranCon 2020.

* Abstract: [abstract.md](./abstract.md)
* Presentation slides: [certik.pdf](./certik.pdf)
* FortranCon LFortran talk webpage: https://tcevents.chem.uzh.ch/event/12/contributions/45/
