# Title

LFortran: Interactive LLVM-based Fortran Compiler for Modern Architectures

# Authors

Ondřej Čertík, ondrej@certik.us, Los Alamos National Laboratory  
Nikhil Maan, nikhilmaan22@gmail.com, Amity University, India  
Ankit Pandey, pandeyan@grinnell.edu, Grinnell College  
Milan Curcic, caomaco@gmail.com, University of Miami  
Peter Brady, ptb@lanl.gov, Los Alamos National Laboratory  
Zach Jibben, zjibben@lanl.gov, Los Alamos National Laboratory  
Neil Carlson, nnc@lanl.gov, Los Alamos National Laboratory  
Rohit Goswami, rog32@hi.is, Science Institute, University of Iceland, VR-III, 107, Reykjavik, Iceland and Department of Chemistry, IIT Kanpur, India  
Amir Shahmoradi, shahmoradi@utexas.edu, the University of Texas Arlington  
Arjen Markus, arjen.markus@deltares.nl, Deltares, The Netherlands  

# Abstract

We are developing a modern open-source Fortran compiler called LFortran
(https://lfortran.org/). This front-end compiler will enable the interactive
execution of code in environments like Jupyter. This will allow exploratory work
(much like Python, MATLAB or Julia) which is currently not feasible. The
interactivity of our compiler does not impede compilation of binaries with the
goal to run user's code on modern architectures such as multi-core CPUs and
GPUs, which is an essential requirement for wider Fortran adoption that current
Fortran compilers do not address well. A Live demo of the compiler with a
Jupyter notebook will be shown. The compiler itself is written in C++ for
robustness and speed with optional Python wrappers to improve inter-operability.
It parses Fortran code to an Abstract Syntax Tree (AST) and transforms it to an
Abstract Semantic Representation (ASR). LFortran has several backends that
transform the ASR to machine code via LLVM, or to C++, or to provide automatic
Python wrappers. More backends are planned. The compiler has been designed to be
modular so that data can be extracted/inserted between the different stages,
which is an important feature that would support an ecosystem of tools that
otherwise would be hard with a monolithic compiler.
